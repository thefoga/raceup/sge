interrupt void CANIntHandler(void) {
    // Read the CAN interrupt status to find the cause of the interrupt
    ulStatus = CANIntStatus(CANA_BASE, CAN_INT_STS_CAUSE);

    // If the cause is a controller status interrupt, then get the status
    if(ulStatus == CAN_INT_INT0ID_STATUS) {
        ulStatus = CANStatusGet(CANA_BASE, CAN_STS_CONTROL);

        bool areThereErrors = ((ulStatus & ~(CAN_ES_TXOK | CAN_ES_RXOK)) != 7) &&
                ((ulStatus & ~(CAN_ES_TXOK | CAN_ES_RXOK)) != 0);
        if (areThereErrors) {
            g_bErrFlag = 1;  // flag that indicates errors
        }
    } else if(ulStatus == 26) {
        CANMessageGet(CANA_BASE, 26, &RX_CAN_AMK_ACT_VAL, true);

        int indexMsg = get_AMK_Motor_index(RX_CAN_AMK_ACT_VAL.ui32MsgID);
        int indexVAL = get_AMK_Motor_index_VAL(RX_CAN_AMK_ACT_VAL.ui32MsgID);

        if(indexVAL == 1) {
            read_AMK_Values1(RX_CAN_AMK_ACT_VAL, indexMsg);
        } else if(indexVAL == 2) {
            read_AMK_Values2(RX_CAN_AMK_ACT_VAL, indexMsg);
        }

        CANIntClear(CANA_BASE, 26);
        g_bErrFlag = 0;
    }

    CANGlobalIntClear(CANA_BASE, CAN_GLB_INT_CANINT0);
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP9;
}
