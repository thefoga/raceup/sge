public class App extends KvaserApp {
    private final MainFrame view;

    public App() {
        this("GUI APP");
    }

    public App(String tag) {
        super(tag);
        view = new MainFrame();
    }

    public static void main(String[] args) {
        try {
            App app = new App();
            app.open();
        } catch (Exception e) {
            showMessage(e);
        }
    }

    private String[] getLaunchSettings() {
        String[] settings = getSettings();
        if (settings == null) {
            new YoloException("User not input settings", ExceptionType
                    .VIEW).print();
            System.exit(1);
        }

        return settings;
    }

    @Override
    protected void setupKvaser() {
        String[] settings = getLaunchSettings();
        String ip = settings[0];
        String bitrate = settings[1];

        boolean logMotors = Boolean.parseBoolean(settings[2]);
        boolean logCan = Boolean.parseBoolean(settings[3]);
        boolean logBattery = Boolean.parseBoolean(settings[4]);
        boolean logIMU = Boolean.parseBoolean(settings[5]);

        hal = new Hal(
                new Motors(),
                new BlackBird(ip)
        );

        try {
            hal.setup(bitrate);
        } catch (Exception e) {
            log(e);
            showMessage(e);
        }

        setupLogUpdaters(logMotors, logCan, logBattery, logIMU);  // add file loggers
    }

    @Override
    protected void setupUpdaters() {
        hal.addObserverToMotors(view.getMotorPanels());
        // todo hal.addObserverToKvaser(view.getCanMessagesFrame());
        // todo hal.addObserverToKvaser(view.getBatteryFrame());
        // todo hal.addObserverToKvaser(view.getDynamicsFrame());
    }

    private void setupLogUpdaters(boolean logMotors, boolean logCan, boolean logBattery, boolean logIMU) {
        if (logMotors) {
            hal.addObserverToMotors(new ShellMotorsUpdater(false, true));
        }

        if (logCan) {
            hal.addObserverToKvaser(new ShellCanUpdater(false, true));
        }

        if (logBattery) {
            hal.addObserverToKvaser(new ShellBatteryUpdater(false, true));
        }

        if (logIMU) {
            hal.addObserverToKvaser(new ShellImuUpdater(false, true));
        }
    }

    @Override
    public void start() {
        hal.start();
    }
}
