void menuBack()
{
    if(start)
    {
        static unsigned long last_interrupt_time = 0;
        unsigned long interrupt_time = millis();
        // If interrupts come faster than 200ms, assume it's a bounce and ignore
        if (interrupt_time - last_interrupt_time > 300)
        {
            if(page > 0)
            {
                // page = display.getPage();
                Serial.println("Page Before: " + String(page));
                page--;
                Serial.println("Page After: " + String(page));
                display.setPage(page);
            }
            Serial.println(String(1));
        }
        last_interrupt_time = interrupt_time;
    }
}

void menuForward()
{
    if(start)
    {
        static unsigned long last_interrupt_time = 0;
        unsigned long interrupt_time = millis();
        // If interrupts come faster than 200ms, assume it's a bounce and ignore
        if (interrupt_time - last_interrupt_time > 300)
        {
            if(page < 2)
            {
                // page = display.getPage();
                Serial.println("Page Before: " + String(page));

                page++;
                page = page%3;
                display.setPage(page);
                Serial.println("Page After: " + String(page));
            }
            Serial.println(String(2));
        }
        last_interrupt_time = interrupt_time;
    }
}

void R2D()
{
    detachInterrupt(BTN3);
    static unsigned long last_interrupt_time = 0;
    unsigned long interrupt_time = millis();

    // If interrupts come faster than 300ms, assume it's a bounce and ignore
    if (interrupt_time - last_interrupt_time > 300)
    {
        CAN_FRAME outgoing;
        outgoing.id = 0x20;
        outgoing.extended = false;
        outgoing.priority = 0; // 0-15 lower is higher priority
        outgoing.length = 1;
        outgoing.data.byte[0] = 0x01;
        Can0.sendFrame(outgoing);

        if(!start)
        {
            start = true;
            display.setPage(page);
            previousMillis = millis();
        }

    }
    
    last_interrupt_time = interrupt_time;
}

void gotFrameCallback(CAN_FRAME *frame)
{
    saveFrame(*frame);
}
