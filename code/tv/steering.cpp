float getInnerTorque(float requestedTorque, float speed, float steering, float k) {
    float absSteer = steering;
    if (absSteer < 0) {
        absSteer *= -1.0;
    }

    float x = changeRange(absSteer, 0, STZ_RANGE, 0, 1);
    x = 1 - x;  // lots of steering -> less torque
    x = changeRange(x, 0, 1, 0.5, 0.8);
    float newTorque = requestedTorque * x;
    return newTorque;
}

float getOuterTorque(float requestedTorque, float speed, float steering, float k) {
    float absSteer = steering;
    if (absSteer < 0) {
        absSteer *= -1.0;
    }

    float x = changeRange(absSteer, 0, STZ_RANGE, 1.1, 1.3);
    float newTorque = requestedTorque * x;
    return newTorque;
}
