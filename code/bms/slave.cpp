byte pec(byte crcBuffer[])
{
    byte crc = 0;
    int temp = 0;
    for (int i = 0; i < 3; i++)
    {
        temp = crc ^ crcBuffer[i];
        crc = crcTable[temp];
    }
    return crc;
}

void bqWrite(byte device_address, byte reg_address, byte reg_data)
{
    delayMicroseconds(5);
    digitalWrite(SLAVE_SELECT_PIN, LOW); // Take the SS pin low to select the chip
    delayMicroseconds(5);

    SPI.setDataMode(SPI_MODE1);
    SPI.beginTransaction(SPISettings(50000, MSBFIRST, SPI_MODE1));

    // Shift the device bit and set bit 0
    byte logical_address = (device_address << 1) | 0x01;
    byte crc_input[3] = {logical_address, reg_address, reg_data};

    // Send and receive SPI
    SPI.transfer(logical_address);
    SPI.transfer(reg_address);
    SPI.transfer(reg_data);
    SPI.transfer(pec(crc_input));

    delayMicroseconds(5);

    digitalWrite(SLAVE_SELECT_PIN, HIGH); // Take the SS pin high to de-select the chip
    SPI.endTransaction();
}

byte *bqRead(byte device_address, byte reg_address, byte length)
{
    delayMicroseconds(5);

    digitalWrite(SLAVE_SELECT_PIN, LOW); // Take the SS pin low to select the chip
    delayMicroseconds(5);
    SPI.beginTransaction(SPISettings(50000, MSBFIRST, SPI_MODE1));

    // Shift the device bit and clear bit 0
    byte logical_address = device_address << 1;
    logical_address &= 0b11111110;

    // Create buffer for receivedData and clear it
    static byte received_data[20];
    memset(received_data, 0, sizeof(received_data));

    // send and receive SPI
    SPI.transfer(logical_address);
    SPI.transfer(reg_address);
    SPI.transfer(length);
    delayMicroseconds(1);
    for (int i = 0; i < length + 1; i++)
    {
        received_data[i] = SPI.transfer(0x00);
    }

    delayMicroseconds(5);

    digitalWrite(SLAVE_SELECT_PIN, HIGH); // Take the SS pin high to de-select the chip
    SPI.endTransaction();
    return received_data;
}

void bqClearAlerts()
{
    // clear alert bit in device status register
    byte *value = bqRead(DEVICE_ADDR, DEVICE_STATUS, 1);
    value[0] |= 0b00100000; // set alert bit as 1
    bqWrite(DEVICE_ADDR, DEVICE_STATUS, value[0]);
    value[0] &= 0b11011111; // clear alert bit
    bqWrite(DEVICE_ADDR, DEVICE_STATUS, value[0]);
    bqWrite(DEVICE_ADDR, ALERT_STATUS, 0xFF);
    byte value_res = 0x00; // Write 0's ALERT_STATUS_REG register
    bqWrite(DEVICE_ADDR, ALERT_STATUS, value_res);
}

void bqClearFaults()
{
    // clear fault bit in device status register
    byte *value = bqRead(DEVICE_ADDR, DEVICE_STATUS, 1);
    value[0] |= 0b01000000; // set fault bit as 1
    bqWrite(DEVICE_ADDR, DEVICE_STATUS, value[0]);
    value[0] &= 0b10111111; // clear fault bit
    bqWrite(DEVICE_ADDR, DEVICE_STATUS, value[0]);
    bqWrite(DEVICE_ADDR, FAULT_STATUS, 0xFF);
    byte value_res = 0x00; // Write 0's FAULT_STATUS_REG register
    bqWrite(DEVICE_ADDR, FAULT_STATUS, value_res);
}

void gpio(bool state)
{
    byte *value = bqRead(DEVICE_ADDR, IO_CONTROL, 2);
    if (state)
    {
        value[0] |= 0b01000000;
    }
    else
    {
        value[0] &= 0b10111111;
    }
    bqWrite(DEVICE_ADDR, IO_CONTROL, value[0]);
}