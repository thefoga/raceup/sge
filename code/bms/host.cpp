sendUpdate("status", -1, "Charging");
sendOperationModeRequest(balancing_mode);

bool charge_completed = false;
monitorBattery();

Serial.print("charge_completed : ");
Serial.print(charge_completed);
Serial.print("\nreceived_command : ");
Serial.print(received_command);
Serial.println();

received_command = false;
while (!charge_completed && !received_command)
{
    double cell_mean_voltage = getAverageVoltage();

    CAN_FRAME outgoing;
    outgoing.id = STATUS_REQUEST_ID;
    outgoing.length = 6; // add a byte to differentiate this packet from balancing mode request packet
    outgoing.data.s0 = 0;
    outgoing.data.s1 = balancing_mode;
    outgoing.data.s2 = (int)cell_mean_voltage;
    Can0.sendFrame(outgoing);

    Serial.println("Sent BALANCE REQUEST");

    delay(TIME_TO_BALANCE);
    monitorBattery();

    for (int i = 1; i <= NUMBER_OF_SLAVES; i++)
    { // check if the charge is completed
        if (isBmsIn(i))
        {
            for (int j = 0; j < NUMBER_OF_CELLS_IN_SLAVE; j++)
            {
                bool voltTooHigh = voltages[i - 1][j] >= MAX_CELL_VOLTAGE - 1;
                bool voltTooLow = (voltages[i - 1][j] <= MIN_CELL_VOLTAGE + 1) && (voltages[i - 1][j] > 0);
                bool shouldStop = voltTooHigh || voltTooLow;

                if (shouldStop)
                {
                    charge_completed = true;
                    Serial.print(i - 1);
                    Serial.print(": ");
                    Serial.print(voltTooHigh);
                    Serial.println(voltTooLow);
                }
            }
        }
    }

    if (Serial.available() > 0)
    { // check if data is present in the serial buffer
        readSerial();
    }
}

received_command = false;

if (charge_completed)
{
    digitalWrite(BMS_FAULT, HIGH); // disable charger
    sendUpdate("status", -1, ">>>>>>>>>>>>>>>> End of charge <<<<<<<<<<<<<<<<");
    delay(CHARGE_DELAY);
    digitalWrite(BMS_FAULT, LOW); // the latch will keep the charger disabled

    normalMode();
}
