void _readImuAccel(tCANMsgObject msg) {
    bytes2int16(msg.pucMsgData[0], msg.pucMsgData[1], &imuAccel.x);
    bytes2int16(msg.pucMsgData[2], msg.pucMsgData[3], &imuAccel.y);
    bytes2int16(msg.pucMsgData[4], msg.pucMsgData[5], &imuAccel.z);

    carPrev.accelerations = carNow.accelerations;  // update car status
    carNow.accelerations = parse(imuAccel);
}

void _readImuOdoVelocity(tCANMsgObject msg) {
    bytes2int32(msg.pucMsgData[0], msg.pucMsgData[1], msg.pucMsgData[2], msg.pucMsgData[3], &imuOdoVelocity.velocity);
}

void _readImuGps1PosInfo(tCANMsgObject msg) {
    bytes2int32(msg.pucMsgData[0], msg.pucMsgData[1], msg.pucMsgData[2], msg.pucMsgData[3], &imuGps1PosInfo.timeUp);
    bytes2int32(msg.pucMsgData[4], msg.pucMsgData[5], msg.pucMsgData[6], msg.pucMsgData[7], &imuGps1PosInfo.status);
}

void _readImuGps1Pos(tCANMsgObject msg) {
    bytes2int32(msg.pucMsgData[0], msg.pucMsgData[1], msg.pucMsgData[2], msg.pucMsgData[3], &imuGps1Pos.latitude);
    bytes2int32(msg.pucMsgData[4], msg.pucMsgData[5], msg.pucMsgData[6], msg.pucMsgData[7], &imuGps1Pos.longitude);
}

void _readImuGps1Alt(tCANMsgObject msg) {
    bytes2int32(msg.pucMsgData[0], msg.pucMsgData[1], msg.pucMsgData[2], msg.pucMsgData[3], &imuGps1Alt.altitude);
    bytes2int16(msg.pucMsgData[4], msg.pucMsgData[5], &imuGps1Alt.altitudeGeo);
    imuGps1Alt.vehicles = msg.pucMsgData[6];
    imuGps1Alt.age = msg.pucMsgData[7];
}
