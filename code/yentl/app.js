function getTestCanvas() {
  let testCanvasHtml = "<form id='questionsForm'>"; // start form
  testCanvasHtml += "<h2>Question #" + (currentQuestionIndex + 1) + "</h2>";
  testCanvasHtml += "<img id='image" + currentQuestionIndex + "' src=''>";
  testCanvasHtml += "<h3>" + questions[currentQuestionIndex]["question"] + "</h3>";

  let answers = questions[currentQuestionIndex]["allAnswers"];
  if (answers.includes('...')) { // this requires user input
    testCanvasHtml += getUserInputAnswerHTML();
  } else {
    testCanvasHtml += getPossibleAnswersHTML(answers);
  }

  // submit button
  if (currentQuestionIndex === questions.length - 1) {
    testCanvasHtml += "<input style='margin-top: 8em;' type='button'" +
      " value='Submit'" +
      " onclick='submitTestAttempt()'></form>";
    testCanvasHtml += "<br><br>";
  }

  // next button
  if (currentQuestionIndex < questions.length - 1) {
    testCanvasHtml += "\n" +
      "        <button style=\"float: right;\" onclick=\"goToNextQuestion()\">Next</button>\n" +
      "    </div><br><br>";
  }

  return testCanvasHtml;
}

function setupTest() {
  questions = getTestQuestions();
  currentQuestionIndex = 0;
}

function populatePage() {
  document.getElementById("test-canvas").innerHTML = getTestCanvas();
  displayImageFromStorage("image" + currentQuestionIndex, "imageData" + currentQuestionIndex);
  loadAnswer();
}

function saveAnswer() {
  let formOptions = $("#questionsForm").find("input");
  let answered = new Set();

  for (let i = 0; i < formOptions.length; i++) {
    if (formOptions[i].type === 'number') {
      answered.add(formOptions[i].value);
    } else if (formOptions[i].type === 'checkbox') {
      if (formOptions[i].checked) {
        answered.add(formOptions[i].value);
      }
    }
  }

  answers[currentQuestionIndex] = answered;
  times[currentQuestionIndex] = questionTime;
}
