cwd            := $(shell pwd)
update         := sudo apt -qq update
install        := sudo apt install -y --no-install-recommends

tex_compile    := pdflatex
tex_file       := main
bib_make       := biber

.PHONY: all view

clean::
	rm -fv *.aux *.log *.bbl *.blg *.toc *.out *.lot *.lof *.dvi *.bcf *.run* # root
	rm -fv */*.aux */*.log */*.bbl */*.blg */*.toc */*.out */*.lot */*.lof */*.dvi */*.bcf */*.run* # root

cleanall::
	$(MAKE) clean
	rm -fv *.pdf

view::
	xdg-open $(tex_file).pdf

install::
	$(update)                                                              && \
	$(install) texlive-fonts-recommended texlive-latex-extra               && \
	$(install) texlive-fonts-extra dvipng texlive-latex-recommended        && \
	$(install) biber

build::
	$(tex_compile) $(tex_file)
	$(bib_make) $(tex_file)
	$(tex_compile) $(tex_file)

rebuild::
	$(MAKE) cleanall
	$(MAKE) build

debug::
	$(MAKE) rebuild
	$(MAKE) view

nac:
	$(MAKE) build
	gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOUTPUTFILE=noActiveContents.pdf $(tex_file).pdf
